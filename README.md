# 3.5e for Foundry VTT - SRD Content

This module for [Foundry Virtual Tabletop](https://foundryvtt.com) provides Open Game Content for its `D35E` system.

The content is adapted from the [3.5 Systems Reference Document](http://web.archive.org/web/20051231195019/www.wizards.com/d20/files/v35/SRD.zip) and used under the terms of the OGL v1.0a, see OGL-1.0a.txt.

The software component of this system is distributed under the MIT license.

## Manual Installation Instructions

This module can be installed directly within Foundry Virtual Tabletop. To install it manually, got to releases page, and copy an URL into the **Install Module** dialog on the Setup menu of the application.
